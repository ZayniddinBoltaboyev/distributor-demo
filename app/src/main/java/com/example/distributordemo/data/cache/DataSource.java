package com.example.distributordemo.data.cache;

import android.content.Context;
import android.content.SharedPreferences;

public class DataSource {

    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    public static void init(Context context) {
        if (preferences == null) {
            preferences = context.getSharedPreferences("clean", Context.MODE_PRIVATE);
            editor = preferences.edit();
        }
    }

    public static void setRegister(boolean isRegister) {
        editor.putBoolean("register", isRegister).apply();
    }

    public static boolean isRegister() {
        return preferences.getBoolean("register", false);
    }

    public static void saveToken(String token) {
        editor.putString("token", token).apply();
    }

    public static String getToken() {
        return preferences.getString("token", "failed");
    }
}
