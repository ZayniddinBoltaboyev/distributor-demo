package com.example.distributordemo;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.distributordemo.model.ClientModel;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

public class ClientsBottomSheetDialog  extends BottomSheetDialogFragment {

    private View root;
    private final Context context;

    private final List<ClientModel> list;


    public ClientsBottomSheetDialog(Context context, List<ClientModel> list) {
        this.context = context;
        this.list = list;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        root = LayoutInflater.from(context).inflate(R.layout.clients_bottom_sheet_fragment, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        RecyclerView clientsRv = root.findViewById(R.id.client_rv);
//
//        RecyclerView.LayoutManager deliveryDetailLm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        clientsRv.setLayoutManager(deliveryDetailLm);
//
//        ClientDiffUtil clientDiffUtil = new ClientDiffUtil();
//        ClientAdapter clientsAdapter = new ClientAdapter(clientDiffUtil,this);
//        clientsAdapter.submitList(list);
//
//        clientsRv.setAdapter(clientsAdapter);
    }


}
